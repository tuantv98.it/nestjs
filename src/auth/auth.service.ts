import {Injectable} from '@nestjs/common';
import {JwtService} from '@nestjs/jwt';
import {UsersService} from 'src/users/users.service';
import * as bcrypt from 'bcrypt';

@Injectable()
export class AuthService {
    constructor(private usersService: UsersService, private jwtTokenService: JwtService) {
    }

    //function hash password
    async hashPassword(password: string): Promise<string> {
        return await bcrypt.hash(password, 12);
    }

    //function compare password param with user password in database
    async comparePassword(password: string, storePasswordHash: string,): Promise<any> {
        return await bcrypt.compare(password, storePasswordHash);
    }

    async validateUserCredentials(email: string, pass: string): Promise<any> {
        const user = await this.usersService.getUserByEmail(email);
        if (user && await this.comparePassword(pass, user.password)) {
            const {password, ...result} = user;
            return result;
        }
        return false;
    }

    async loginWithCredentials(user: any) {
        const payload = {email: user.email, sub: user.userId};

        return {
            access_token: this.jwtTokenService.sign(payload),
        };
    }
}