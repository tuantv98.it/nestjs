import {Module} from '@nestjs/common';
import {JwtModule} from '@nestjs/jwt';
import {PassportModule} from '@nestjs/passport';
import {UsersModule} from 'src/users/users.module';
import {AuthService} from './auth.service';
import {jwtConstants} from './constants';
import {JwtStrategy} from './jwt.strategy';
import {UsersService} from "../users/users.service";
import {AuthController} from "./auth.controller";
import {UsersRepository} from "../users/users.repository";
import {ConfigService} from "@nestjs/config";
import {StorageAmazonBucketsS3} from "../support/StorageAmazonBucketsS3";

@Module({
    imports: [
        UsersModule,
        PassportModule,
        JwtModule.register({
            secret: jwtConstants.secret,
            signOptions: {expiresIn: '3600s'}
        })],
    providers: [AuthService, JwtStrategy, UsersService, UsersRepository, ConfigService, StorageAmazonBucketsS3],
    controllers: [AuthController],
    exports: [AuthService]
})
export class AuthModule {
}