import {Injectable} from '@nestjs/common';
import {UsersRepository} from "./users.repository";
import * as bcrypt from 'bcrypt';
import {Users} from "./users.model";
import {ConfigService} from "@nestjs/config";
import {S3} from 'aws-sdk';
import {v4 as uuid} from 'uuid';
import {StorageAmazonBucketsS3} from "../support/StorageAmazonBucketsS3";
import {Constants} from "../constants/constant";

@Injectable()
export class UsersService {
    constructor(private readonly usersRepository: UsersRepository,
                private readonly configService: ConfigService,
                private readonly storageAmazon: StorageAmazonBucketsS3
    ) {
    }

    async create(body) {
        const checkExistUser = this.usersRepository.findOne({
            where: {
                email: body.email
            }
        })

        if (checkExistUser) {
            return false
        }

        const saltOrRounds = Constants.saltOrRounds;
        const password = await bcrypt.hash(body.password, saltOrRounds);
        const path = Constants.pathSaveS3

        const uploadResult = await this.storageAmazon.uploadFileTos3(body.image, path)

        const users = {
            'password': password,
            'user_name': body.user_name,
            'email': body.email,
            'name': body.name,
            'image': uploadResult.Location
        }

        return new Promise(async (resolve, reject) => {
            try {
                const data = this.usersRepository.save(users);
                resolve(data)
            } catch (e) {
                console.log(e);
                reject(e)
            }
        })
    }

    async getUserByEmail(email: string) {
        return await this.usersRepository.findOne({
            where: {
                email: email
            }
        });
    }

    async getUsers() {
        return new Promise((resolve, reject) => {
            try {
                const data = this.usersRepository.find();
                resolve(data);
            } catch (e) {
                reject(e);
            }
        });
    }
}
