import {Body, Controller, Get, Post, Req, Res, UseGuards} from "@nestjs/common";
import {CategoryService} from "./category.service";
import {response} from "../support/ResponseResource";
import {Request, Response} from "express";
import {CreateCategory} from "./category.model";
import {JwtAuthGuard} from "../auth/jwt-auth.guard";

@Controller("cate")
export class CategoryController {
    constructor(private readonly categoryService: CategoryService) {
    }

    @UseGuards(JwtAuthGuard)
    @Get()
    async getHello(@Req() req: Request, @Res() res: Response) {
        const data = await this.categoryService.getList();
        return response(res, data);
    }

    @Post("create")
    async creaetdPost(@Body() body: CreateCategory, @Res() res: Response) {
        const data = await this.categoryService.create(body);
        console.log(data);
        return response(res, data)
    }
}

