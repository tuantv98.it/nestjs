import {DataSource, EntityRepository, Repository} from 'typeorm'
import {Category} from "./category.model";

@EntityRepository(Category)
export class CategoryRepository extends Repository<Category> {

    constructor(dataSource: DataSource) {
        super(Category, dataSource.createEntityManager());
    }

    getInactivePosts() {
        console.log(this);
        return this.find({
            relations: {
                post: true
            }
        })
    }
}
