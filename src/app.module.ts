import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { PostModule } from "./post/post.module";
import { ConfigModule } from "@nestjs/config";
import { TypeOrmModule } from '@nestjs/typeorm';
import { Post } from "./post/post.model";
import { isString } from "@nestjs/common/utils/shared.utils";
import { Category } from "./category/category.model";
import { CategoryModule } from "./category/category.module";
import {Users} from "./users/users.model";
import {UsersModule} from "./users/users.module";
import { NestjsFormDataModule } from 'nestjs-form-data';
import {AuthModule} from "./auth/auth.module";

@Module({
  imports: [PostModule,CategoryModule,UsersModule,AuthModule,ConfigModule.forRoot(),
    TypeOrmModule.forRoot({
    type:  'mysql',
    host: process.env.DB_HOST,
    port: 3306,
    username: process.env.DB_USERNAME,
    password: process.env.DB_PASSWORD,
    database: process.env.DB_DATABASE,
    entities: [Post,Category,Users],
    synchronize: true,
  }),],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule {}
